import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminUsuarioRoutingModule } from './admin-usuario-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminUsuarioRoutingModule
  ]
})
export class AdminUsuarioModule { }
