import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home"
  },
  {
    path: "home",
    loadChildren: () => import("./components/home/home.module").
    then(m => m.HomeModule)
  },
  {
    path: "cadautor",
    loadChildren: () => import ("./components/cad-autor/cad-autor.module").
    then(m => m.CadAutorModule)
  },
  {
    path: "cadeditora",
    loadChildren: () => import ("./components/cad-editora/cad-editora.module").
    then(m => m.CadEditoraModule)
  },
  {
    path: "cadlivro",
    loadChildren: () => import ("./components/cad-livro/cad-livro.module").
    then(m => m.CadLivroModule)
  },
  {
    path: "cadusuario",
    loadChildren: () => import ("./components/cad-usuario/cad-usuario.module").
    then(m => m.CadUsuarioModule)
  },
  {
    path: "cadendereco",
    loadChildren: () => import ("./components/cad-endereco/cad-endereco.module").
    then(m => m.CadEnderecoModule)
  },
  {
    path: "cadfuncionario",
    loadChildren: () => import ("./components/cad-funcionario/cad-funcionario.module").
    then(m => m.CadFuncionarioModule)
  },

  {
    path: "**",
    loadChildren: () => import("./components/page-not-found/page-not-found.module")
    .then(m => m.PageNotFoundModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
