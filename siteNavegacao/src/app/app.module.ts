import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadAutorComponent } from './components/cad-autor/cad-autor.component';
import { CadEditoraComponent } from './components/cad-editora/cad-editora.component';
import { CadLivroComponent } from './components/cad-livro/cad-livro.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LivrosService } from './services/livros/livros.service';
import { AutoresService } from './services/autores/autores.service';
import { AdminUsuarioComponent } from './admin/admin-usuario/admin-usuario.component';
import { LoginComponent } from './admin/login/login.component';
import { CadUsuarioComponent } from './components/cad-usuario/cad-usuario.component';
import { CadEnderecoComponent } from './components/cad-endereco/cad-endereco.component';
import { CadFuncionarioComponent } from './components/cad-funcionario/cad-funcionario.component';

@NgModule({
  declarations: [
    AppComponent,
    CadAutorComponent,
    CadEditoraComponent,
    CadLivroComponent,
    PageNotFoundComponent,
    AdminUsuarioComponent, 
    LoginComponent,
    CadUsuarioComponent,
    AdminUsuarioComponent,
    CadEnderecoComponent,
    CadFuncionarioComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    LivrosService,
    AutoresService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
