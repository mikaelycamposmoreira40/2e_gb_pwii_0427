import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadFuncionarioRoutingModule } from './cad-funcionario-routing.module';
import { CadFuncionarioComponent } from './cad-funcionario.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadFuncionarioRoutingModule
  ]
})
export class CadFuncionarioModule { }
