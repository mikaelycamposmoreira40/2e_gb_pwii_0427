import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadUsuarioRoutingModule } from './cad-usuario-routing.module';
import { CadUsuarioComponent } from './cad-usuario.component';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadUsuarioRoutingModule
  ]
})
export class CadUsuarioModule { }
