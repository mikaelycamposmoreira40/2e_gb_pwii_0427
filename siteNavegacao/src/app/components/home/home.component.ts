import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/models/Autor';
import { AutoresService } from 'src/app/services/autores/autores.service';
import { LivrosService } from 'src/app/services/livros/livros.service';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  autores: Autor[]

  constructor(
    private livrosService: LivrosService,
    private autoresService: AutoresService,
    private usuariosService: UsuariosService
  ) {
    this.autores = []
  }

  ngOnInit(): void {
    this.autoresService.buscarTodosOsAutores()
    .subscribe({
      next: (resposta: any) => {
        this.autores = resposta.results

        console.log(this.autores)
      },
      error: (erro: any) => {
        console.error(erro)
      }
    })

    this.livrosService.buscarLivros()
    .subscribe({
      next: (livros) => {
        console.log(livros)
      },
      error: (erro) => {
        console.error(erro)
      },
    })

    this.usuariosService.buscarUsuarios()
      .subscribe({
        next: (usuario) => {
          console.log(usuario)
        },
        error: (erro) => {
          console.error(erro)
        }
      })
    }
  }