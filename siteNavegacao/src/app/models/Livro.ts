export class Livro{
    titulo: String;
    anoPublicacao: number;
    edicao: number;
    idioma: String;
}