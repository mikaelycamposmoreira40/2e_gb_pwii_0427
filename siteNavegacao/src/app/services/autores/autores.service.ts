import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  private readonly URL_Y = "https://3000-profdanilom-2egbapi0810-cv1d1of8e6o.ws-us77.gitpod.io/"
  private readonly URL_M = "https://3000-profdanilom-2egbapi0810-cv1d1of8e6o.ws-us78.gitpod.io/"
  private readonly URL = this.URL_Y

  constructor(
    private http: HttpClient
  ) { }

  buscarTodosOsAutores(): Observable<any> {
    return this.http.get<any>(`${this.URL}autores`)
  }
}
