import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LivrosService {
  private readonly URL_M = "https://3000-yasmimlinda-2egbapi0810-i9hptreb5pm.ws-us77.gitpod.io/"
  private readonly URL_Y = "https://3000-yasmimlinda-2egbapi0810-cpwosksr0jr.ws-us77.gitpod.io/"
  private readonly URL = this.URL_Y

  constructor(
    private http : HttpClient
  ) { }

  buscarLivros(): Observable<any>{
    return this.http.get<any>(`${this.URL}livros`)
  }
}
