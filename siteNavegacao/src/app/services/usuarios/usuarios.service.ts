import { Observable } from 'rxjs';
import { Usuario } from './../../../../../../2e_gb_pwii_0427/siteNavegacao/src/app/models/Usuario';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_M = "https://3000-yasmimlinda-2egbapi0810-jtvkbrkg4kg.ws-us78.gitpod.io/"
  private readonly URL_Y = "https://3000-yasmimlinda-2egbapi0810-jtvkbrkg4kg.ws-us78.gitpod.io/"
  private readonly URL = this.URL_M

  constructor(
    private readonly http: HttpClient
  ) { }

  logar(usuario: Usuario): Observable<any> {
    return this.http.post<any>(`${this.URL}usuario/login`, usuario)
  }

  cadastrar(usuario: Usuario): Observable<any> {
    return this.http.post<any>(`${this.URL}usuario`, usuario)
  }

  buscarUsuarios(): Observable<any> {
    return this.http.get<any>(`${this.URL}usuarios`)
  }
}
